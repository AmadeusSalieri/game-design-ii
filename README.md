# Objectif
Sélectionner un genre de jeu classique et y ajouter un twist. Ce twist sera exploité dans des situations différentes qui utilisent un nombre de fonctionnalités réduit.

# Rendu
-	Soutenance – Rendu de base :
-	Une démo du jeu (3 à 5mn de jeu) propre et polishée.
-	Le groupe d'étudiant-es jouera à la démo en la commentant.
-	Présentation de 10mn sur le jeu, explicitant :
-	Le genre sélectionné et le twist.
-	L'exploitation de ce twist.
-	La boucle de gameplay.
-	Les variables de difficulté et leur modulation dans les situations de jeu.
-	La direction artistique.
-	Les bases des 3C et de la gestion de la caméra.
-	Un plan ou explicatif de Level design.
-	La liste des principaux signes & feeedbacks.
-	Bonus pour les étudiant-es souhaitant aller plus loin :
-	Design : Identifier un stéréotype vu en cours ou en dehors et en proposer une utilisation innovante.
-	Technique : Développer de manière particulièrement pointue un ou plusieurs feedbacks pour les amener à un niveau de qualité technique avancée.
