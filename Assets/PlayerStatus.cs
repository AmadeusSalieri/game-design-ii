using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI; //Allows us to use UI.
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStatus : MonoBehaviour
{
    public int boss = 0;
    public int Life = 2;
    public Text _life;

    public GameObject Base;
    
    public GameObject Guerrier;
    
    private bool Sam = false;
    public GameObject Samurai;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    
    public void TakeDamge(int damage)
    {
        Life -= damage;
    }

    // Update is called once per frame
    void Update()
    {
        _life.text = "" + Life;
        
        
        if (Input.GetKeyDown("e"))
        {
            if (Sam == true)
            {
                Samurai.SetActive(true);
                Base.SetActive(false);
            }
        }
        
        if (Life <= 0)
        {
            SceneManager.LoadScene("Menu");
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);

        if (other.tag == "samurai")
        {
            Sam = true;
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "samurai")
        {
            Sam = false;
            //pnj = null;
        }
    }
    
}
