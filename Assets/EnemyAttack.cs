using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private float timeBtwAttack = 0;
    public float startTimeBtwAttack = 1f;
    public float incantationTime = 0.01f;

    public Transform attackPos;
    public float attackRange;
    public LayerMask WhatIsEnemies;

    int damage = 1;
    private Animator animator;					//Used to store a reference to the Player's animator component.

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        damage = 1 + GameObject.Find("Player").GetComponent<Player>().boss;


        if (timeBtwAttack <= 0)
        {
            Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, WhatIsEnemies);
            if (enemiesToDamage != null)
            {
                incantationTime -= Time.deltaTime;
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    if (enemiesToDamage[i].GetComponent<Player>() && incantationTime <= 0)
                    {
                        animator.SetTrigger("AttackTrigger");
                        enemiesToDamage[i].GetComponent<Player>().TakeDamge(damage);
                        incantationTime = 0.01f;
                    }
                }
            }
            timeBtwAttack = startTimeBtwAttack;
        }
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            incantationTime = 0.01f;
        }
    }

}
