using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DieScript : MonoBehaviour
{
    // Start is called before the first frame update
    void DieIsTriggered()
    {
        SceneManager.LoadScene("Menu");
    }
}
