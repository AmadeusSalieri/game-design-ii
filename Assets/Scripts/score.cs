using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour
{
    public static int scoreValue = 0;

    Text _score;
    void Start()
    {
        
        _score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _score.text = ""+scoreValue;
    }
}
