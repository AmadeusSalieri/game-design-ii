using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_attack : MonoBehaviour
{
    private float timeBtwAttack = 0;
    public float startTimeBtwAttack = 0.3f;

    public Transform attackPos;
    public float attackRange;
    public LayerMask WhatIsEnemies;

    public int damage;
    public AudioSource sword;

    public GameObject Samurai;
    public GameObject Guerrier;

    void Update()
    {

        if (timeBtwAttack <= 0)
        {

            if (Input.GetKeyDown("space"))
            {
                Samurai.GetComponent<Animator>().SetTrigger("AttackTrigger");
                Guerrier.GetComponent<Animator>().SetTrigger("AttackTrigger");
                sword.Play();
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, WhatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                   
                    enemiesToDamage[i].GetComponent<hits>().TakeDamge(damage);
                }
            }
            timeBtwAttack -= startTimeBtwAttack;
        }
        else
        {
            timeBtwAttack = Time.deltaTime;
        }
    }
}
