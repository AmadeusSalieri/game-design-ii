using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hits : MonoBehaviour
{
    public bool isBoss = false;
    public int life = 10;
    public int point = 10;
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (life <= 0)
        {
            score.scoreValue += point;
            if (isBoss)
            {
                GameObject.Find("ExitBlocks").SetActive(false);
                GameObject.Find("Player").GetComponent<Player>().boss++;
            }
            Destroy(gameObject);
        }
    }

    public void TakeDamge(int damage)
    {
        life -= damage;
    }
}
