﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject parent;

    public AudioClip moveSound1; //1 of 2 Audio clips to play when player moves.
    public AudioClip moveSound2; //2 of 2 Audio clips to play when player moves.
    public AudioClip gameOverSound; //Audio clip to play when player dies.


    private BoxCollider2D boxCollider2D;
    private SpriteRenderer spriteRenderer;

    public float movementSpeed;

    public int damage = 5;
    public int scale = 1;
    public bool alreadyFlipped = false;
    public int boss = 0;
    public int Life = 2;
    public Text _life;

    public GameObject[] Base;

    public int pnj = 0;


    private bool Sam = false;
    private bool Guerrier = false;
    
    public Transform rightAttackPos;
    public Transform leftAttackPos;


    //Start overrides the Start function of MovingObject
    void Start()
    {
        //Get a component reference to the Player's animator component

        boxCollider2D = GetComponent<BoxCollider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        if (horizontalInput == 0 && verticalInput == 0)
        {
            Base[pnj].GetComponent<Animator>().SetBool("Moving", false);
        }
        else
        {
            Base[pnj].GetComponent<Animator>().SetBool("Moving", true);
        }

        Base[pnj].GetComponent<Animator>().SetFloat("Horizontal", horizontalInput);
        Base[pnj].GetComponent<Animator>().SetFloat("Vertical", verticalInput);
        Vector3 _position = new Vector3(horizontalInput * movementSpeed, verticalInput * movementSpeed, 0.0f);

        parent.transform.position = transform.position + _position * Time.deltaTime;

        // Flip the character towards correct direction
        if (horizontalInput < 0)
        {
            Base[pnj].GetComponent<SpriteRenderer>().flipX = !alreadyFlipped;
            gameObject.GetComponent<player_attack>().attackPos = leftAttackPos;
        }
        else if (horizontalInput > 0)
        {
            Base[pnj].GetComponent<SpriteRenderer>().flipX = alreadyFlipped;
            gameObject.GetComponent<player_attack>().attackPos = rightAttackPos;
        }

        _life.text = "" + Life;


        if (Input.GetKeyDown("e"))
        {
            if (Sam == true)
            {
                Base[1].SetActive(true);
                Base[2].SetActive(false);
                Base[0].SetActive(false);
                movementSpeed = 9;
                damage = 5;
                Life += 1;
                pnj = 1;
            }

            if (Guerrier == true)
            {
                Base[2].SetActive(true);
                Base[1].SetActive(false);
                Base[0].SetActive(false);
                damage = 10;
                movementSpeed = 5;
                Life += 1;
                pnj = 2;
            }
        }

        if (Life <= 0)
        {
            Base[pnj].GetComponent<Animator>().SetTrigger("DeathTrigger");
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Guerrier")
        {
            Guerrier = true;
        }

        if (other.tag == "samurai")
        {
            Sam = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "samurai")
        {
            Sam = false;
            //pnj = null;
        }

        if (collision.tag == "Guerrier")
        {
            Guerrier = false;
            //pnj = null;
        }
    }

    public void TakeDamge(int damage)
    {
        Life -= damage;
    }

    //Restart reloads the scene when called.
    private void Restart()
    {
        //Load the last scene loaded, in this case Main, the only scene in the game. And we load it in "Single" mode so it replace the existing one
        //and not load all the scene object in the current scene.
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
    }
}