using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public Transform player; // changed this to Transform
    public float detectRange = 2; // this gets multiplied by itself to compare to a sqr magnitude check (instead of distance)
    public bool inRange = false;
    public float scale = 1;
    public float moveSpeed = 2f; // you can adjust this, of course.
    private Animator animator;

    void Awake()
    {
        player = GameObject.Find("Player").transform;
        detectRange *= detectRange;
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (player)
        {
            float distsqr = (player.position - transform.position).sqrMagnitude;
            if (distsqr < detectRange)
            {
                
                // Flip towardks direction
                Vector3 localScale = transform.localScale;
                if (transform.position.x < player.position.x)
                {
                    localScale.x = scale * -1;
                }
                else
                {
                    localScale.x = scale * 1;
                }

                transform.localScale = localScale;
                
                Vector2 translation = Vector2.MoveTowards(transform.position, player.position, moveSpeed * Time.deltaTime);
                if (translation.Equals(transform.position))
                {
                    animator.SetBool("Moving", false);
                }
                else
                {
                    animator.SetBool("Moving", true);
                }
                transform.position = translation;
            }   
        }
    }

}
