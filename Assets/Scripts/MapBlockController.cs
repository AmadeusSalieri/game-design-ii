using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBlockController : MonoBehaviour
{
    public int width;
    public int height;
    public int minEnemies;
    public int maxEnemies;
    private MapSideCollider sideCollider;

    // Enemies prefabs ?

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < transform.childCount; i++) {
            GameObject child = transform.GetChild(i).gameObject;

            if (child.tag == "SideCollider") {
                sideCollider = child.GetComponent<MapSideCollider>();
                sideCollider.SetParent(gameObject);
            }
        }
    }

    public void SetGenerateAction(System.Action<GameObject> callback)
    {
        if (sideCollider) {
            sideCollider.SetGenerateAction(callback);
        }
    }

    void GenerateEnemies()
    {
        // TODO
    }
}
