using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSideCollider : MonoBehaviour
{
    private GameObject parent;
    private System.Action<GameObject> generateAction;
    private bool triggered = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void SetParent(GameObject parentObj)
    {
        parent = parentObj;
    }

    public void SetGenerateAction(System.Action<GameObject> action)
    {
        generateAction = action;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && !triggered) {
            triggered = true;
            generateAction(parent);
        }
    }
}
