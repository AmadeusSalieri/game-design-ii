using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneratorController : MonoBehaviour
{
    public int difficulty = 0;
    public int levelLength = 5;
    public int nextBossIn = 5;
    public List<GameObject> blockPrefabs;
    public GameObject bossBlockPrefab;
    public List<GameObject> bossBlocksByDifficulty;
    private Queue<GameObject> blocks;
    private System.Action<GameObject> generateAction;

    // Start is called before the first frame update
    void Start()
    {
        blocks = new Queue<GameObject>();
        generateAction = new System.Action<GameObject>(Generate);
        GameObject startBlock = GameObject.Find("BlockStart");

        startBlock.GetComponent<MapBlockController>().SetGenerateAction(generateAction);
        blocks.Enqueue(startBlock);
    }

    void Generate(GameObject last)
    {
        Vector2 position = new Vector2(last.transform.position.x + last.GetComponent<MapBlockController>().width, last.transform.position.y);
        GameObject prefab = SelectBlock();
        GameObject block = Instantiate(prefab, position, Quaternion.identity) as GameObject;

        block.GetComponent<MapBlockController>().SetGenerateAction(generateAction);

        blocks.Enqueue(block);
    }

    private GameObject SelectBlock()
    {
        if (nextBossIn == 0)
        {
            nextBossIn = 10;
            return bossBlockPrefab;
        }
        nextBossIn -= 1;

        int index = Random.Range(0, blockPrefabs.Count - 1);

        return blockPrefabs[index];
    }
}
