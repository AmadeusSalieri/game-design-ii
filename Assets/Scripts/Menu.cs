using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{
  public void GetPlay()
   {
       SceneManager.LoadScene("LevelOne");
   }

   public void GetQuit()
    {
      Application.Quit ();
    }

    public void GetHelp()
     {
         SceneManager.LoadScene("Help");
     }
     public void GetBacktoMenu()
      {
          SceneManager.LoadScene("Menu");
      }
}
