using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBlockScript : MonoBehaviour
{
    public void TriggerExitBoss()
    {
        GameObject.Find("ExitBlocks").SetActive(false);
    }
}
